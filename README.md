# Node exporter

Ansible role for Prometheus node exporter. For use with [these playbooks](https://gitlab.com/stemid-ansible/ansible-prometheus.git).
